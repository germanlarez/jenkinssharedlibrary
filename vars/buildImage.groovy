#!/usr/bin/env groovy

def call(String imageName) {
    echo "building Image from jenkinsSharedLibrary"
    withCredentials([usernamePassword(credentialsId: 'DockerHub-Repo', passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]) {
        sh "docker build -t $imageName ."
        sh "echo $PASSWORD | docker login -u $USERNAME --password-stdin"
        sh "docker push $imageName"
    }
}
