#!/usr/bin/env groovy

def call() {
    echo "building jar from jenkinsSharedLibrary."
    sh 'mvn clean'
    sh 'mvn package'
}
