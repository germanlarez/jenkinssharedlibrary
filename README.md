# Jenkins shared library
___

#### This goes along with this repo:

[Jenkins Shared library repository](https://gitlab.com/germanlarez/jenkins.git)
\
Branch `jenkinsSharedLibrary-pipeline`

#### Project structure is:
- `vars` folder where are all the functions that we're calling from jenkinsfile
- `src` folder where we would have any utility or helpers for those functions.

Create 1 .groovy file per function you have in each jenkins file stage

#### In example if you script.groovy is:
    
    def buildJar() {
        echo "building the application..."
        sh 'mvn package'
        }
    
    def buildImage() {
        echo "building the docker image..."
        withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh 'docker build -t glarez/freestylejob-dockerimagepush:jma-2.0 .'
        sh "eho $PASSWORD | docker login -u $USERNAME --password-stdin"
        sh 'docker push glarez/freestylejob-dockerimagepush:jma-2.0'
        }
        }
    
    def deployApp() {
        echo 'deploying the application...'
        }
    
    return this

#### You will have `vars/buildJar.groovy` and `vars/buildJar.groovy` created in your library:

    #!/usr/bin/env groovy
    
    def call() {
        echo "building the application..."
        sh 'mvn clean'
        sh 'mvn package'
        }

#### That should be your `buildJar.groovy` and so...

#### In Jenkins server go to:

Manage Jenkins/Configure System/Global Pipeline Libraries
fill:
- Name: It could be you repo name for the library, but it's not mandatory. Name it in a descriptive name as usual. IE: `JenkinsSharedLibrary`
- Default version: could be a branch name or a commit hash, IE: `master`or `3532cd224740325677a51e1e0f0190b046fe2749`
- Modern SCM: Checked.
- Declare you library's repo, IE: `https://gitlab.com/germanlarez/jenkinssharedlibrary.git`
- Configure the credentials for the repo.

### We are all set!

### To make use of the library:

 please refer to [Jenkins pipeline Shared library repository](https://gitlab.com/germanlarez/jenkins.git) 
 \
 Branch `Jenkins-sharedLibrary-pipelines`

## Using Parameters in Shared Library

We have hardcoded the build tag of our Docker image in `buildImage.groovy`:

      #!/usr/bin/env groovy      
      def call() {
       echo "building Image from jenkinsSharedLibrary"
       withCredentials([usernamePassword(credentialsId: 'DockerHub-Repo', passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]) {
        sh 'docker build -t glarez/freestylejob-dockerimagepush:jma-2.0 .'
        sh 'echo $PASSWORD | docker login -u $USERNAME --password-stdin'
        sh 'docker push glarez/freestylejob-dockerimagepush:jma-2.0'
        }
       }

We should define parameters in call() like this:

      #!/usr/bin/env groovy
      def call(String imageName) {
       echo "building Image from jenkinsSharedLibrary"
       withCredentials([usernamePassword(credentialsId: 'DockerHub-Repo', passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]) {
        sh "docker build -t $imageName ."
        sh "echo $PASSWORD | docker login -u $USERNAME --password-stdin"
        sh "docker push $imageName"
        }
       }

Another thing we can also do is access all the Jenkins global variables to check from which branch is the application building from:

    #!/usr/bin/env groovy
    
    def call() {
    echo "building jar jenkinsSharedLibrary from $BRANCH_NAME"
    sh 'mvn clean'
    sh 'mvn package'
    }

In your Jenkins file, you can make use of this parameters like this:

        stage("build jar") {
            steps {
                script {
                    buildJar()
                }
            }
        }
        stage("build image") {
            steps {
                script {
                    buildImage 'glarez/freestylejob-dockerimagepush:jma-3.0'
                }
            }
        }
